#!/usr/bin/env bash

export PIP_CONFIG_FILE=/app/.pip/pip.conf


rm -fr git_repo
git clone https://$USER:$PASS@bitbucket.org/taykey/$REPO.git git_repo

export PYTHONPATH=./git_repo/$REPO/tests/:./git_repo/:./git_repo/tests/
echo "***********************************************************"
echo $PYTHONPATH
echo "***********************************************************"


### Install any needed packages specified in requirements.txt
pip install -r git_repo/requirements.txt
#pip install --trusted-host pypi.python.org -r $REPO/requirements.txt
#start cron jobs
service cron start &

### Run pdoc as http server
pdoc --http --only-pypath --all-submodules --http-port $PDOCPORT --http-host 0.0.0.0


